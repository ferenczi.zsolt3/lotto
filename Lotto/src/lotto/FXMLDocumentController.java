/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lotto;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;

/**
 *
 * @author Zsolt
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TextField input1;
    @FXML
    private TextField input2;
    @FXML
    private TextField input3;
    @FXML
    private TextField input4;
    @FXML
    private TextField input5;

    @FXML
    private Label sorsolt1;
    @FXML
    private Label sorsolt2;
    @FXML
    private Label sorsolt3;
    @FXML
    private Label sorsolt4;
    @FXML
    private Label sorsolt5;
    @FXML
    private Label eredmeny;

    private final int MIN = 1;
    private final int MAX = 90;

    int[] lista = new int[5];
    int[] talalat = new int[5];
    int[] generalt = new int[5];

    public boolean NotNullChecking() {

        if (input1.getText().isEmpty() || input2.getText().isEmpty() || input3.getText().isEmpty() || input4.getText().isEmpty() || input5.getText().isEmpty()) {
            System.out.println("Nem �rt�l be sz�mot");
            JOptionPane.showMessageDialog(null, "Nem �rt�l be sz�mot!", "HIBA!", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    public boolean Betuellenorzes() {
        try {
            if (Integer.parseInt(input1.getText()) < MIN || Integer.parseInt(input1.getText()) > MAX) return false;
            if (Integer.parseInt(input2.getText()) < MIN || Integer.parseInt(input2.getText()) > MAX) return false;
            if (Integer.parseInt(input3.getText()) < MIN || Integer.parseInt(input3.getText()) > MAX) return false;
            if (Integer.parseInt(input4.getText()) < MIN || Integer.parseInt(input4.getText()) > MAX) return false;
            if (Integer.parseInt(input5.getText()) < MIN || Integer.parseInt(input5.getText()) > MAX) return false;
        
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Val�sz�n? bet?t adt�l meg","Hiba", JOptionPane.ERROR_MESSAGE);
            //System.out.println("Hiba: "+e);
        }
        return true;
    }

    public boolean EgyformaSzamEllenorzes() {

        int elsoszam = Integer.parseInt(input1.getText());
        int masodikszam = Integer.parseInt(input2.getText());
        int harmadikszam = Integer.parseInt(input3.getText());
        int negyedikszam = Integer.parseInt(input4.getText());
        int otodikszam = Integer.parseInt(input5.getText());

        lista[0] = elsoszam;
        lista[1] = masodikszam;
        lista[2] = harmadikszam;
        lista[3] = negyedikszam;
        lista[4] = otodikszam;

        for (int i = 0; i < 5 - 1; i++) {
            for (int j = i + 1; j < 5; j++) {
                if (lista[i] == lista[j]) {
                    JOptionPane.showMessageDialog(null, "Be�rt �rt�kek k�z�tt van egyforma", "Hiba", JOptionPane.ERROR_MESSAGE);
                    return false;
                }

            }

        }

        return true;
    }

    public void sorsolas() {
        for (int i = 0; i < 5; i++) {
            generalt[i] = 0;
        }

        //Ha m�r l�tezik ilyen sz�m, akkor �jra sorsol rekurz�van
        for (int i = 0; i < 5; i++) {
            int gen = (int) (Math.random() * MAX) + MIN;
            for (int j = 0; j < 5; j++) {
                if (gen == generalt[j]) {
                    sorsolas();
                }
            }
            generalt[i] = gen;
        }
    }

    //nem lehet nagyobb, mint 90, �s kisebb, mint 1 1-90 k�z�tt legyen;
    public boolean hibaszures() {
        for (int i = 0; i < 5; i++) {
            if (lista[i] < MIN || lista[i] > MAX) {
                JOptionPane.showMessageDialog(null, "A be�rt sz�m nem lehet nagyobb, mint " + MAX + " vagy kisebb, mint " + MIN, "HIBA", JOptionPane.ERROR_MESSAGE);
                return false;
            }

        }

        return true;
    }

    @FXML
    private void handleButtonAction(ActionEvent event) {

        eredmeny.setText("");
        Betuellenorzes();
        //Kiv�telek lekezel�se, bet?re m�g nincs kezel�s
        if (NotNullChecking() == true && EgyformaSzamEllenorzes() == true && hibaszures() == true) {

            //v�letlen sz�m gener�l�s
            sorsolas();

            sorsolt1.setText(Integer.toString(generalt[0]));
            sorsolt2.setText(Integer.toString(generalt[1]));
            sorsolt3.setText(Integer.toString(generalt[2]));
            sorsolt4.setText(Integer.toString(generalt[3]));
            sorsolt5.setText(Integer.toString(generalt[4]));

            //tal�latok ellen?rz�se
            talalat();
        }

    }

    public void talalat() {

        for (int i = 0; i < 5; i++) {
            talalat[i] = generalt[i];
        }

        int talalatszam = 0;
        String talalt = "";

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (lista[i] == talalat[j]) {
                    talalatszam++;
                    talalt += lista[i] + " ";
                }
            }
        }

        if (talalatszam > 0) {
            eredmeny.setText(talalatszam + " Tal�latod van, a " + talalt + " Gratul�lok!");
            //JOptionPane.showMessageDialog(null, talalatszam + " Tal�latod van", "Nyerem�nyInf�", JOptionPane.PLAIN_MESSAGE);
        } else {
            eredmeny.setText("Sajnos most nem nyert�l");
            //JOptionPane.showMessageDialog(null, "Sajnos most nem nyert�l", "Nyerem�nyinf�", JOptionPane.PLAIN_MESSAGE);
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
